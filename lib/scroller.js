'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _lodash = require('lodash.debounce');

var _lodash2 = _interopRequireDefault(_lodash);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Scroller = function () {
    function Scroller() {
        _classCallCheck(this, Scroller);

        this.SCROLLERCONST = {
            scrollingThreshold: 150,
            scrollingMinimalValue: 0,
            swippingThreshold: 350,
            swippingMinimalValue: 50
        };
        this.config = {
            eventType: false,
            direction: false,
            intensity: false,
            scrollX: false,
            scrollY: false,
            callbackDelay: 0
        };
        this.datas = {};
        this.callbackFunction = null;
        this.touchGesture = {};
        this.lastTime = 0;
        this.intensityValue = null;
        this.onPause = true;
        this.scrollEvent = null;
    }

    _createClass(Scroller, [{
        key: '_preventScrolling',
        value: function _preventScrolling(event) {
            event = event || window.event;
            event.preventDefault();
        }
    }, {
        key: '_getEventType',
        value: function _getEventType(eventType) {
            return eventType;
        }
    }, {
        key: '_getDirection',
        value: function _getDirection(deltaX, deltaY, minimalValue) {
            var direction = {};
            if (Math.abs(deltaX) > Math.abs(deltaY)) {
                deltaX >= minimalValue ? direction = 'right' : direction = 'left';
                this.intensityValue = deltaX;
            } else {
                deltaY >= minimalValue ? direction = 'down' : direction = 'up';
                this.intensityValue = deltaY;
            }
            return direction;
        }
    }, {
        key: '_getIntensity',
        value: function _getIntensity(intensityValue, threshold) {
            intensityValue = Math.abs(intensityValue);
            if (intensityValue > threshold) {
                intensityValue = threshold;
            }
            this.scrollingInfo.intensity = Math.ceil(intensityValue / (threshold / 10)) / 10;
            return intensityValue;
        }
    }, {
        key: '_getScrollX',
        value: function _getScrollX() {
            return window.pageXOffset || document.documentElement.scrollLeft;
        }
    }, {
        key: '_getScrollY',
        value: function _getScrollY() {
            return window.pageYOffset || document.documentElement.scrollTop;
        }
    }, {
        key: '_scrollingInfoConfiguration',
        value: function _scrollingInfoConfiguration(event, eventType) {
            this.scrollEvent = event;
            if (this.onPause) {
                event.preventDefault();
                event.stopImmediatePropagation();
            } else {

                var threshold = this.SCROLLERCONST[eventType + 'Threshold'],
                    minimalValue = this.SCROLLERCONST[eventType + 'MinimalValue'],
                    deltaX = event.deltaX,
                    deltaY = event.deltaY;

                if (eventType === 'swipping') {
                    deltaX = this.touchGesture.touchstart.x - this.touchGesture.touchend.x;
                    deltaY = this.touchGesture.touchstart.y - this.touchGesture.touchend.y;
                }

                if (this.config.eventType) {
                    this.datas.eventType = this._getEventType(eventType);
                }

                if (this.config.direction) {
                    this.datas.direction = this._getDirection(deltaX, deltaY, minimalValue);
                }

                if (this.config.intensity) {
                    var intensityValue = void 0;
                    if (this.intensityValue === null) {
                        this._getDirection(deltaX, deltaY, threshold, minimalValue);
                    }
                    intensityValue = this.intensityValue;
                    this.datas.intensity = this._getIntensity(intensityValue, threshold);
                    this.intensityValue = null;
                }

                if (this.config.scrollX) {
                    this.datas.scrollX = this._getScrollX();
                }

                if (this.config.scrollY) {
                    this.datas.scrollY = this._getScrollY();
                }

                this.callbackFunction(this.datas);
                this.pauseDelay();
            }
        }
    }, {
        key: 'pauseDelay',
        value: function pauseDelay() {
            var _this = this;

            this.onPause = true;
            window.setTimeout(function () {
                _this.onPause = false;
                window.clearTimeout();
            }, this.config.callbackDelay);
        }
    }, {
        key: 'startScroller',
        value: function startScroller() {
            window.onwheel = null;
            window.ontouchmove = null;
            this.onPause = false;
        }
    }, {
        key: 'stopScroller',
        value: function stopScroller() {
            window.onwheel = this._preventScrolling; // modern standard
            window.ontouchmove = this._preventScrolling; // mobile
            this.onPause = true;
        }
    }, {
        key: 'init',
        value: function init(callback, config) {
            var _this2 = this;

            this.config.eventType = config.eventType;
            this.config.direction = config.direction;
            this.config.intensity = config.intensity;
            this.config.scrollX = config.scrollX;
            this.config.scrollY = config.scrollY;
            this.config.callbackDelay = config.callbackDelay;
            this.callbackFunction = callback;
            this.startScroller();

            document.addEventListener('wheel', (0, _lodash2.default)(function (event) {
                _this2._scrollingInfoConfiguration(event, 'scrolling');
            }, 40, { leading: true, trailing: false }));

            document.addEventListener('touchstart', function (event) {
                _this2.touchGesture.touchstart = {
                    x: event.changedTouches[0].screenX,
                    y: event.changedTouches[0].screenY
                };
            });
            document.addEventListener('touchend', (0, _lodash2.default)(function (event) {

                _this2.touchGesture.touchend = {
                    x: event.changedTouches[0].screenX,
                    y: event.changedTouches[0].screenY
                };
                _this2._scrollingInfoConfiguration(event, 'swipping');
            }, 40, { leading: true, trailing: false }));
        }
    }]);

    return Scroller;
}();

exports.default = Scroller;