# Scroller

Scroller allow you to control and get info back from scrolling and swipping.

Scroller can send you back différent info:

- The event type (scrolling, swipping)
- The direction (down, up, left, right).
- The velocity (between 0 and 1).
- The ScrollX / SwipeX value.
- The ScrollY / SwipeY value.

## Dependencies 

Scroller has a dependency over lodash is debounce function

## Scroller API

To load scroller you have to instanciate it like this : 

```
import Scroller from '../scroller/Scroller';

let scroller = new Scroller();

```

The you can call the Init function in order to initialize it : 

```
scroller.init(func, [requestedInfo = {}]);
```
- **func (Function):** callback function that will be called when a scroll event is trigger.
- **requestedInfo = {} (object):** Object déclaring the info you want back from scroller

### Usage : 

```
scroller.init(mycallBack, {
    direction: true,
    velocity: true
})
```

### Others public methods :

**startScroller** : Restart scroller aftter it wass stopped

```
scroller.startScroller();
```

**stopScroller** : stop scroller when a break is neeeded

```
scroller.stopScroller();
```