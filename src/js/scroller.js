import debounce from 'lodash.debounce';

export default class Scroller {
    constructor() {
        this.SCROLLERCONST = {
            scrollingThreshold: 150,
            scrollingMinimalValue: 0,
            swippingThreshold: 350,
            swippingMinimalValue: 50
        };
        this.config = {
            eventType: false,
            direction: false,
            intensity: false,
            scrollX: false,
            scrollY: false,
            callbackDelay: 0
        };
        this.datas = {};
        this.callbackFunction = null;
        this.touchGesture = {};
        this.lastTime = 0;
        this.intensityValue = null;
        this.onPause = true;
        this.scrollEvent = null;
    }

    _preventScrolling(event) {
        event = event || window.event;
        event.preventDefault();

    }


    _getEventType(eventType) {
        return eventType;
    }
    _getDirection(deltaX, deltaY, minimalValue) {
        let direction = {};
        if (Math.abs(deltaX) > Math.abs(deltaY)) {
            deltaX >= minimalValue ? direction = 'right' : direction = 'left';
            this.intensityValue = deltaX;
        } else {
            deltaY >= minimalValue ? direction = 'down' : direction = 'up';
            this.intensityValue = deltaY;
        }
        return direction;
    }
    _getIntensity(intensityValue, threshold) {
        intensityValue = Math.abs(intensityValue);
        if (intensityValue > threshold) {
            intensityValue = threshold;
        }
        this.scrollingInfo.intensity = Math.ceil(intensityValue / (threshold / 10)) / 10;
        return intensityValue;
    }
    _getScrollX() {
        return window.pageXOffset || document.documentElement.scrollLeft
    }
    _getScrollY() {
        return window.pageYOffset || document.documentElement.scrollTop
    }
    _scrollingInfoConfiguration(event, eventType) {
        this.scrollEvent = event;
        if (this.onPause) {
            event.preventDefault();
            event.stopImmediatePropagation();
        } else {

            let threshold = this.SCROLLERCONST[eventType + 'Threshold'],
                minimalValue = this.SCROLLERCONST[eventType + 'MinimalValue'],
                deltaX = event.deltaX,
                deltaY = event.deltaY;

            if (eventType === 'swipping') {
                deltaX = this.touchGesture.touchstart.x - this.touchGesture.touchend.x;
                deltaY = this.touchGesture.touchstart.y - this.touchGesture.touchend.y;
            }

            if(this.config.eventType) {
                this.datas.eventType = this._getEventType(eventType);
            }

            if(this.config.direction) {
                this.datas.direction = this._getDirection(deltaX, deltaY, minimalValue);
            }

            if(this.config.intensity) {
                let intensityValue;
                if (this.intensityValue === null) {
                    this._getDirection(deltaX, deltaY, threshold, minimalValue);
                }
                intensityValue = this.intensityValue;
                this.datas.intensity = this._getIntensity(intensityValue, threshold);
                this.intensityValue = null;
            }

            if(this.config.scrollX) {
                this.datas.scrollX = this._getScrollX();
            }

            if(this.config.scrollY) {
                this.datas.scrollY = this._getScrollY();
            }

            this.callbackFunction(this.datas);
            this.pauseDelay();
        }

    }

    pauseDelay() {
        this.onPause = true;
        window.setTimeout(() => {
            this.onPause = false;
            window.clearTimeout();
        }, this.config.callbackDelay)
    }

    startScroller() {
        window.onwheel = null;
        window.ontouchmove = null;
        this.onPause = false;
    }
    stopScroller() {
        window.onwheel = this._preventScrolling; // modern standard
        window.ontouchmove = this._preventScrolling; // mobile
        this.onPause = true;
    }
    init(callback, config) {
        this.config.eventType = config.eventType;
        this.config.direction = config.direction;
        this.config.intensity = config.intensity;
        this.config.scrollX = config.scrollX;
        this.config.scrollY = config.scrollY;
        this.config.callbackDelay = config.callbackDelay;
        this.callbackFunction = callback;
        this.startScroller();

        document.addEventListener('wheel', debounce((event) => {
            this._scrollingInfoConfiguration(event, 'scrolling');
        }, 40, { leading: true, trailing: false }));

        document.addEventListener('touchstart', (event) => {
            this.touchGesture.touchstart = {
                x: event.changedTouches[0].screenX,
                y: event.changedTouches[0].screenY
            };
        });
        document.addEventListener('touchend', debounce((event) => {

            this.touchGesture.touchend = {
                x: event.changedTouches[0].screenX,
                y: event.changedTouches[0].screenY
            };
            this._scrollingInfoConfiguration(event, 'swipping');
        }, 40, { leading: true, trailing: false }));
    }
}
